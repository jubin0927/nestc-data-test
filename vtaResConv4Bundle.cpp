

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaResConv4MainEntry.h"
SymbolTableEntry symbolTableEntry_vtaResConv4Bundle[2]={{"inputP",0,100352,'1'},{"outP",100352,100352,'1'}};
BundleConfig vtaResConv4Bundle_config = {147968, 200704, 0, 64, 2, symbolTableEntry_vtaResConv4Bundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaResConv4MainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(147456);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 147456, 1);
  biasP = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 147456), 0, biasP, 0, 512, 1);
}

void vtaResConv4MainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaResConv4MainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 100352;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(100352);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 28, 28, 128);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 28, 28, 128, 128, 3, 3, 1, 1, 0, 1, 7, 28, 28, vtaCmdH, 2, 14, 28);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 28, 28, 128 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}