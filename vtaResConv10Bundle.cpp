

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaResConv10MainEntry.h"
SymbolTableEntry symbolTableEntry_vtaResConv10Bundle[2]={{"inputP",0,25088,'1'},{"outP",25088,25088,'1'}};
BundleConfig vtaResConv10Bundle_config = {2361344, 50176, 0, 64, 2, symbolTableEntry_vtaResConv10Bundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaResConv10MainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 2359296, 1);
  biasP = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 2359296), 0, biasP, 0, 2048, 1);
}

void vtaResConv10MainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaResConv10MainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 25088;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(25088);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 7, 7, 512);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 7, 7, 512, 512, 3, 3, 1, 1, 0, 1, 7, 7, 7, vtaCmdH, 2, 7, 7);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 7, 7, 512 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}