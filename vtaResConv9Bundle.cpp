

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaResConv9MainEntry.h"
SymbolTableEntry symbolTableEntry_vtaResConv9Bundle[2]={{"inputP",0,50176,'1'},{"outP",50176,25088,'1'}};
BundleConfig vtaResConv9Bundle_config = {1181696, 75264, 0, 64, 2, symbolTableEntry_vtaResConv9Bundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaResConv9MainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(1179648);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 1179648, 1);
  biasP = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1179648), 0, biasP, 0, 2048, 1);
}

void vtaResConv9MainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaResConv9MainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 50176;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 14, 14, 256);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 14, 14, 256, 512, 3, 3, 1, 2, 0, 1, 8, 7, 7, vtaCmdH, 2, 7, 7);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 7, 7, 512 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}