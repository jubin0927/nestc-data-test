

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaResConv7MainEntry.h"
SymbolTableEntry symbolTableEntry_vtaResConv7Bundle[2]={{"inputP",0,50176,'1'},{"outP",50176,50176,'1'}};
BundleConfig vtaResConv7Bundle_config = {590848, 100352, 0, 64, 2, symbolTableEntry_vtaResConv7Bundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaResConv7MainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(589824);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 589824, 1);
  biasP = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 589824), 0, biasP, 0, 1024, 1);
}

void vtaResConv7MainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaResConv7MainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 50176;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 14, 14, 256);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 14, 14, 256, 256, 3, 3, 1, 1, 0, 1, 7, 14, 14, vtaCmdH, 2, 14, 14);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 14, 14, 256 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}